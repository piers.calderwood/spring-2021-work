<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Piers Calderwood" />
  <title>Reproducing Sutton (1988)</title>
  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      word-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title"> Reproducing Sutton (1988)</h1>
<p class="author">Piers Calderwood</p>
</header>
<h1 id="abstract">Abstract</h1>
<p>When learning to predict an outcome from a sequence of states, the typical approach is to learn from the difference between predicted and actual final outcome. Temporal Difference (TD) learning is a natural extension to this which assigns credit between successive predictive states, rather than a single final outcome. This is a more efficient and incremental usage of the data which can reduce convergence time and the amount of data required to converge. In this paper I will replicate the experimental results presented in <span class="citation" data-cites="Sutton:1988"></span> which demonstrate TD’s advantages.</p>
<h1 id="introduction">Introduction</h1>
<p>When a system needs to predict the outcome of a sequence of states, e.g. learning to predict the weather on Saturday using the previous 6 day’s weather data, the typical approach is to wait until Saturday to observe the weather and adjust the model based on the result. This approach is referred to as Widrow-Hoff.</p>
<p>TD learning adjusts the predictions about the sequential states before the final outcome is known. In the weather example, the model can adjust its prediction for Friday’s weather on Friday, which will give a decent estimate of Saturday’s weather.<span class="citation" data-cites="Sutton:1988"></span>. This incremental approach is also useful as it allows online learning and reduces the memory requirements as the whole sequence doesn’t need to be stored in memory for training to occur.</p>
<p>Part of the description of TD learning is a new hyper parameter, <span class="math inline"><em>λ</em></span>, which controls the amount of weight prior state-predictions affect model adjustment. In the extreme, <span class="math inline"><em>λ</em> = 1</span> is identical to Widrow-Hoff, as each past state equally affects the update. Smaller <span class="math inline"><em>λ</em></span> assign less credit to earlier states in the sequence. At <span class="math inline"><em>λ</em> = 0</span>, only the most recent prediction affects model update. The resulting algorithm is referred to as TD<span class="math inline">(<em>λ</em>)</span>.</p>
<p>Conceptually, the TD(<span class="math inline"><em>λ</em></span>) algorithm can be thought of as a weighted combination of all n-step look ahead models, that is models which use the past n-steps to build their estimate. At the extremes, TD(0) is a 1-step look ahead and TD(1) is the infinite-step look ahead with intermediary values of <span class="math inline"><em>λ</em></span> being weighed proportional to <span class="math inline"><em>λ</em></span> and <span class="math inline"><em>n</em></span>. <span class="citation" data-cites="Sutton:1988"></span></p>
<figure>
<img src="RandomWalk.png" id="fig:fig3" alt="Random walk MDP" /><figcaption aria-hidden="true">Random walk MDP</figcaption>
</figure>
<h1 id="problem-overview">Problem Overview</h1>
<p>There are three experiments I replicated from Sutton (1988). Each imposes different conditions on the agents for the same learning problem.</p>
<p>The agent needs to learn the state-values, that is the expected reward, of a Markov Decision Process (MDP) from sets of random walks. The specific MDP is the 7-state MDP pictured in Figure 1. Each non-terminal state has an equal chance of transitioning to any of its neighbors. The two terminal states, <span class="math inline"><em>A</em></span> and <span class="math inline"><em>G</em></span>, result in a reward of <span class="math inline">0</span> and <span class="math inline">1</span> respectively. The ideal state value for each of the non-terminal states are <span class="math inline">[<sup>1</sup>&frasl;<sub>6</sub>, <sup>2</sup>&frasl;<sub>6</sub>, <sup>3</sup>&frasl;<sub>6</sub>, <sup>4</sup>&frasl;<sub>6</sub>, <sup>5</sup>&frasl;<sub>6</sub>]</span>. The agent’s model is a linear model of the non-terminal state-values, initialized to <span class="math inline">0.5</span> in each entry to maintain consistency.</p>
<p><span class="math inline">1, 000</span> random walks were split into <span class="math inline">100</span> equally sized training sets. They were reused for all agents and experiments to maintain consistency. The random seed is fixed so exact sequence are reproducible between runs.</p>
<p>The random walk serves as an ideal example as it is simple to analyze by hand, with easy to compute ideal state values. It also shows the advantages of TD learning over Widrow-Hoff, as will be demonstrated in the experiments.</p>
<p>The experiments will adjust the hyper parameters <span class="math inline"><em>λ</em></span> and <span class="math inline"><em>α</em></span>. <span class="math inline"><em>λ</em></span> refers to the <span class="math inline"><em>λ</em></span> in TD(<span class="math inline"><em>λ</em></span>), which is used to train the agent. <span class="math inline"><em>α</em></span> is the learning rate and affects the rate of convergence by scaling how much the underlying model is adjusted. Larger learning rates lead to faster convergence but may "overstep" the correct value, leading to a thrashing behavior. We record the resulting TD agent’s root mean square error (RMSE) of agent’s estimate of the state-values and the ideal.</p>
<p>Lastly, I will discuss the efficiency of each agent/model in two ways. The first is sample efficiency, which relates to the number of examples the model would need to eventually converge to the ideal. If the underlying process is expensive to generate examples from then sample efficiency is important to consider. The other efficiency is training time, which refers to the amount of time spent learning. If a model can accurately estimate the ideal after 10 examples but needs to be presented them 10,000 times it is sample efficient but inefficient at training time.</p>
<figure>
<img src="fig3.png" id="fig:fig3" alt="Replication of Experiment 1/Figure 3 in Sutton (1988)." /><figcaption aria-hidden="true">Replication of Experiment 1/Figure 3 in Sutton (1988).</figcaption>
</figure>
<h1 id="experiment-1">Experiment 1</h1>
<p>Experiment 1 trains 7 agents with a <span class="math inline"><em>λ</em></span> between <span class="math inline">0</span> and <span class="math inline">1</span> (Widrow-Hoff) on all 100 datasets. Model updates were performed after presenting each set. The hypothesis is that a lower <span class="math inline"><em>λ</em></span> will better utilize the limited data and can converge to a better estimate of the state-values. In the experiment, each agent is trained until it converges within some tolerance. Sutton trained them to <span class="math inline"><em>σ</em> = 0.01</span>; I took this to mean until the underlying model’s weights changed no more than <span class="math inline">0.01</span> between batches. Because the exact meaning of convergence provided by Sutton was unclear, for each model in my experiment I presented the entire dataset a large fixed number of times and used a considerably smaller alpha. This process will still result in the agent converging to a fixed solution. The final result is the model’s error compared to the ideal prediction.</p>
<p>For this experiment, the lowest error occurs at <span class="math inline"><em>λ</em> = 0</span> and increases with <span class="math inline"><em>λ</em></span>, peaking at <span class="math inline"><em>λ</em> = 1</span>. This matches the hypothesis, that lower <span class="math inline"><em>λ</em></span> agents were more data efficient. With fewer samples they are more accurately able to estimate the underlying MDP. If data collection is difficult or expensive, a lower lambda would be a better choice if your only concern is reducing error. However, this did come at a cost to training time. The TD<span class="math inline">(1)</span> agents managed to converge in as little as 12 viewings of the dataset while TD<span class="math inline">(0)</span> agents took around 70 presentations before converging.</p>
<p>We expect to see the lowest error at TD(0) as well as the longest training time. As an example, state <span class="math inline"><em>D</em></span> will occur in every sequence, often multiple times. In the limit, it will occur in as many <span class="math inline">0</span> reward sequences as <span class="math inline">1</span> reward sequences. However, on the limited data there will be a slight bias to sequences ending in <span class="math inline"><em>A</em></span> or <span class="math inline"><em>G</em></span>. For TD(1), we would expect it to converge slightly above for below <span class="math inline">0.5</span> based on which terminal state the dataset is biased towards.</p>
<p>TD(0) on the other hand will be able to exploit the fact that <span class="math inline"><em>D</em> → <em>C</em></span> and <span class="math inline"><em>D</em> → <em>E</em></span> occurs more frequently as it can occur multiple times in a sequence. This will lead to a better estimate of the true transition probabilities. The downside of this is that for the model to converge it’s estimate of <span class="math inline"><em>D</em></span>, it first needs to converge estimates of <span class="math inline"><em>C</em></span> and <span class="math inline"><em>E</em></span>. This will take longer as the information provided by <span class="math inline"><em>A</em></span> and <span class="math inline"><em>G</em></span> need to be "walked" back state by state.</p>
<p>Both Sutton and my results share the same shaped curve, parabolic. Low <span class="math inline"><em>λ</em></span> agents have a roughly comparable error which grows higher and faster as <span class="math inline"><em>λ</em></span> increases. However, a discrepancy between my results and Sutton’s is the scale of the error, my <span class="math inline"><em>λ</em> = 1</span> is below Sutton’s <span class="math inline"><em>λ</em> = 0</span>. The most plausible explanation I have for this is it is due to modern floating point precision. When I enforced 16 bit floats and would consistently get underflows, my final results used double precision floats. It is possible the precision used by Sutton was not high enough in 1988 to achieve a lower bound in 1988.</p>
<figure>
<img src="fig4.png" id="fig:fig3" alt="Replication of Experiment 2/Figure 4 in Sutton (1988)." /><figcaption aria-hidden="true">Replication of Experiment 2/Figure 4 in Sutton (1988).</figcaption>
</figure>
<h1 id="experiment-2">Experiment 2</h1>
<p>In experiment 2, I trained 4 TD agents using different learning rates, <span class="math inline"><em>α</em></span>. Instead of training to convergence on all data, the model is trained using a single dataset of 10 sequences. 100 models for each pair of <span class="math inline">(<em>λ</em>,<em>α</em>)</span> are trained, 1 per dataset. The error is the average error across those 100 models. The <span class="math inline"><em>λ</em></span> are <span class="math inline">0, 0.3, 0.8,</span> and <span class="math inline">1.0</span>, and <span class="math inline"><em>α</em></span> ranges from <span class="math inline">[0,0.6]</span> in <span class="math inline">0.05</span> increments. When <span class="math inline"><em>α</em> = 0</span> no training occurs, the weight vector will remain at <span class="math inline">0.5</span> for all positions, and all models have the same error. Of note, if the average error exceeds the <span class="math inline"><em>α</em> = 0</span> estimate I will refer to it as "diverged". The model would either require a smaller <span class="math inline"><em>α</em></span> or more data. Sutton is not specific on when model updates occur. In my experiments the model is updated after each example is presented.</p>
<p>The hypothesis of this experiment is that lower <span class="math inline"><em>λ</em></span> will be able to use a larger learning rate to converge more quickly to the ideal even with extremely limited data. This would show two things: 1. the low <span class="math inline"><em>λ</em></span> agent is more sample efficient and 2. the rate of convergence can be increased with a large initial <span class="math inline"><em>α</em></span> because it is more stable.</p>
<p>In my results the lowest error occurs at <span class="math inline"><em>λ</em> = 0.3</span>, with a slightly lower average error at <span class="math inline"><em>α</em> = 0.2</span> than <span class="math inline"><em>λ</em> = 0</span>. The two highest <span class="math inline"><em>λ</em></span> do not achieve a low error close to this. The worst performing is TD(1) which diverges with an <span class="math inline"><em>α</em> = 0.15</span>. All models eventually diverge. The <span class="math inline"><em>α</em></span> these occur at are all different but are inversely related with the magnitude of <span class="math inline"><em>λ</em></span>. <span class="math inline"><em>λ</em> = 1</span> diverges first with a small <span class="math inline"><em>α</em> = 0.15</span>, <span class="math inline"><em>λ</em> = 0.8</span> next at <span class="math inline"><em>α</em> = 0.35</span>, quickly followed by <span class="math inline"><em>λ</em> = 0</span> and <span class="math inline">0.3</span> at <span class="math inline"><em>α</em> = 0.4</span> and <span class="math inline">0.5</span>.</p>
<p>Both my graph and Sutton’s are similar. The lowest error occurs for the same <span class="math inline"><em>λ</em></span> and achieves approximately the same error of 0.1. They are also in agreement that all models will diverge with a sufficiently large <span class="math inline"><em>α</em></span> and the rough rate of divergence is the same between <span class="math inline"><em>λ</em></span>.</p>
<p>There are three discrepancies of note. Firstly, the best <span class="math inline"><em>α</em></span> for <span class="math inline"><em>λ</em> = 0</span> and <span class="math inline"><em>λ</em> = 0.3</span> in my experiment is 0.2. In Sutton’s it occurs at 0.3. Similarly, the best <span class="math inline"><em>α</em></span> for TD(0.8) is 0.1 in my experiment and 0.15 in Sutton’s. A related effect is the models also diverge earlier for these <span class="math inline"><em>λ</em></span>. In Sutton’s experiment, TD(0.3) does not diverge (although it probably would for <span class="math inline"><em>α</em> &gt; 0.6</span>) while mine does at 0.5. One possible explanation is that my datasets happen to favor these lower <span class="math inline"><em>α</em></span>. This would need to be tested by changing the underlying dataset and observing if there is a change in error for TD(0) and TD(0.3)</p>
<p>The other difference is the lowest error rates for TD(0) and TD(0.3) are almost exactly the same in my experiment. In Sutton’s experiments, TD(0.3) is noticeably lower. Again, I would guess that my dataset happens to favor lower <span class="math inline"><em>λ</em></span> more than Sutton’s did.</p>
<p>This shows that the "best" <span class="math inline"><em>λ</em></span> isn’t necessarily 0 given limited training data and training time. As shown in Experiment 1, the lower <span class="math inline"><em>λ</em></span> takes longer for information to propagate to early states. There is a trade off with sample efficiency and training time. A low <span class="math inline"><em>λ</em></span>, like 0 or 0.3, takes fewer samples but more training time, which is overcome in this experiment by using a large <span class="math inline"><em>α</em></span>.</p>
<figure>
<img src="fig5.png" id="fig:fig3" alt="Replication of Experiment 3/Figure 5 in Sutton (1988)." /><figcaption aria-hidden="true">Replication of Experiment 3/Figure 5 in Sutton (1988).</figcaption>
</figure>
<h1 id="experiment-3">Experiment 3</h1>
<p>In the final experiment, I repeated the training in experiment 2, 100 agent with a fixed <span class="math inline"><em>λ</em></span> trained on 10 sequences each but using the <span class="math inline"><em>α</em></span> which yields the lowest error. The graph will show the best possible outcome for each <span class="math inline"><em>λ</em></span> value. This can be thought of as repeating experiment two with more values of <span class="math inline"><em>λ</em></span> and choosing the minimum error for each <span class="math inline"><em>λ</em></span>. This is precisely how I implemented the experiment. Sutton does not say the optimal <span class="math inline"><em>α</em></span>’s he obtained or the range he tested with, so the same range of <span class="math inline"><em>α</em></span> were used as in experiment 2. From experiment 2, the error rate between similar <span class="math inline"><em>α</em></span> is close enough that testing with smaller increments would not greatly change the minimal error. The hypothesis is that on limited training time and data, a non-zero <span class="math inline"><em>λ</em></span> will be optimal because it makes the best trade-off between sample efficiency and training efficiency.</p>
<p>In my graph we see the expected behavior. There is a lowpoint, at TD(0.2). And as indicated by the previous experiment, the curve grows rapidly as the value of <span class="math inline"><em>λ</em></span> increases. While less pronounced than in Sutton, the error also increases for <span class="math inline"><em>λ</em> &lt; 0.2</span>.</p>
<p>My graph and Sutton’s also show that, while there may be an optimal <span class="math inline"><em>λ</em></span>, being "close" is sufficient for a good model. When comparing Sutton’s and my graphs, any <span class="math inline"><em>λ</em></span> between 0.1 and 0.4 are close to optimal with the actual optimal probably lying between 0.2 and 0.3. This is a quite generous range. We would expect that for other problems there is a range of <span class="math inline"><em>λ</em></span> which give "good enough" results. When building the agent, only a few <span class="math inline"><em>λ</em></span> would need to be tested which reduces the complexity when tuning hyper-parameters.</p>
<p>While the shape is very roughly the same in both my and Sutton’s graphs, there are 3 discrepancies. The first is that we had different low points. Mine occurred at TD(0.2) while his occurred at TD(0.3). This leads into the seconds discrepancy, the error at TD(0) is much lower which changes the curves shape, flattening it at the lower end. In Sutton there is a more significant increase in error at 0. The reason for these differences must be the same as in experiment 2, since the error difference between TD(0) and TD(0.3) were approximately the same for their optimal <span class="math inline"><em>α</em></span>. A change in datasets might lead to a larger <span class="math inline"><em>λ</em></span> being optimal.</p>
<p>Lastly, for all <span class="math inline"><em>λ</em></span> the error is lower than the corresponding model in Sutton’s graph. e.g. in Sutton, TD(1) has an error of approximately 0.20, while mine has an error slightly under 0.19. More pronounced is TD(0.2) which has an error slightly between 0.10 and 0.11 in Sutton and is clearly below 0.10 in mine. Like in experiment 1, the cause is possibly attributable to the difference in floating point precision.</p>
<h1 id="conclusion">Conclusion</h1>
<p>These three experiments demonstrate that for sequential learning, TD provides advantages over Widrow-Hoff. Experiment 1 shows the sample efficiency of a low <span class="math inline"><em>λ</em></span> with the trade off of increased training time. Experiments 2 and 3 show that the "optimal" <span class="math inline"><em>λ</em></span> isn’t necessarily at 0 and how the "optimal" is specific to both the amount of data available and how long it can be trained on. TD(<span class="math inline"><em>λ</em></span>) allows for a tuneable model that trades off between these two extremes. For a fixed amount of data and training time, the experiments show there will be an optimal TD(<span class="math inline"><em>λ</em></span>) better than Widrow-Hoff and all other <span class="math inline"><em>λ</em></span>. It should be noted that there are some additional advantages of TD that are not demonstrated by these experiments, such as online learning and extensions to minimax and using other gradient based model, like neural nets. <span class="citation" data-cites="Sutton:1988"></span></p>
<div class="thebibliography">
<p><span>99</span></p>
<p>Sutton, R.S. Mach Learn (1988) 3: 9. https://doi.org/10.1007/BF00115009 Learning to predict by the methods of temporal differences</p>
</div>
</body>
</html>
