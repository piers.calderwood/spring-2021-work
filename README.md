# Spring 2021 Work

Summaries of my spring 2021 school work. Code is available upon request but isn't public to avoid studen plagarism. Similiarly, I cannot provide a rubrik or project details beyond what is inferable from the project contents.

The first project reproduces the examples in Sutton's 1988 paper. [Learning to Predict by the Method of Temporal Differences](http://incompleteideas.net/papers/sutton-88-with-erratum.pdf)

The second project discuss my solution to the [Open AI "Lunar Lander" environment.](https://gym.openai.com/envs/LunarLander-v2/)

The third and final project reproduces the game and results of [Greenwald 2003](https://www.aaai.org/Papers/ICML/2003/ICML03-034.pdf).