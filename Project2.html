<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Piers Calderwood" />
  <title>Solving Open AI's Lunar Lander</title>
  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      word-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title">Solving Open AI's Lunar Lander</h1>
<p class="author">Piers Calderwood</p>
</header>
<h1 id="abstract">Abstract</h1>
<p>Generalization in unfamiliar states is an important property of any successful agent. Without the ability to generalize, an agent would be unable to act in unfamiliar situations or in continuous spaces. In this paper, I will be applying Q-Learning and neural networks in order to solve the lunar lander environment, which asks an agent to land a space ship on a landing pad using the ships thrust controls. I will explain the solution as well as the tuning required for an agent to solve the environment as well as document the agent’s failures.</p>
<h1 id="environment">Environment</h1>
<p>The lunar lander environment is an environment provided in the OpenAI Gym <span class="citation" data-cites="Brockman:2016"></span>. The intended goal is for an agent to pilot and safely land a space ship onto a landing pad, located at the origin. The environment has discrete time steps and after providing an observation to the agent waits for an action before advancing. An observation, or environment state, is an array consisting of the location, speed, and orientation of the ship in <span class="math inline">ℝ<sup>2</sup></span> and two binary features, one for each leg if it is in contact with the ground.</p>
<p>At each time step, the agent can make 1 of 4 actions: do nothing or fire the left, the right, or the primary thruster which exerts a fixed amount of acceleration on the ship according to the ships orientation.</p>
<p>There are 4 sources of reward from the environment. The first is for moving onto the landing pad and coming to a rest. This reward is potential-based, which means it is lost in equal amounts for moving away from the pad. The potential reward varies between 100-140 depending on the start location. The second source of reward is a large amount after coming to a rest depending on if the ship crashes or lands safely, which yield -100 and +100 respectively. Firing a thruster, the third reward, costs -0.3. A final source of reward is for each leg in contact with the ground, up to +20. Each source of reward incentivizes the agent to both land quickly and safely on the landing pad.</p>
<p>An agent is considered to have solved the environment if it scores an average of 200+ points per episode for 100 consecutive episodes. As will be apparent later, it is also important to verify the agent doesn’t overfit during training or get lucky. After training, the agent should still perform at least as good for another 100 episodes.</p>
<figure>
<img src="0_agent.png" id="fig:fig1" alt="Agent in environment" /><figcaption aria-hidden="true">Agent in environment</figcaption>
</figure>
<h1 id="the-agent">The Agent</h1>
<p>My agent uses <span class="math inline"><em>Q</em></span>-learning with a neural network. While not deep enough to be called Deep Q-Learning, the agent takes much inspiration from the paper <em>Playing Atari with Deep Reinforcement Learning</em> <span class="citation" data-cites="Minh:2013"></span>. The <span class="math inline"><em>Q</em></span>-function is a function over State-Action pairs which should return the, possibly discounted, expected reward for performing action <span class="math inline"><em>A</em></span> from state <span class="math inline"><em>S</em></span>. If the <span class="math inline"><em>Q</em></span>-function is optimal (<span class="math inline"><em>Q</em><sup>*</sup></span>), an agent can perform optimally by choosing the action which maximizes the function at the current state. The <span class="math inline"><em>Q</em></span>-function is recursively defined at each update cycle, <span class="math inline"><em>i</em></span>.</p>
<p><span class="math display"><em>Q</em><sub><em>i</em> + 1</sub> ← <em>Q</em><sub><em>i</em></sub>(<em>s</em><sub><em>t</em></sub>,<em>a</em><sub><em>t</em></sub>) + <em>α</em> ⋅ <em>T</em><em>D</em></span> where <span class="math display"><em>T</em><em>D</em> = <em>r</em><sub><em>t</em></sub> + <em>γ</em>max<sub><em>a</em></sub><em>Q</em><sub><em>i</em></sub>(<em>s</em><sub><em>t</em> + 1</sub>,<em>a</em>) − <em>Q</em><sub><em>i</em></sub>(<em>s</em><sub><em>t</em></sub>,<em>a</em><sub><em>t</em></sub>)</span></p>
<p>The Q update is a temporal difference update, which gives it the benefits of incremental and online learning. <span class="citation" data-cites="Sutton:1988"></span></p>
<p>Internally, the agent uses a neural network as a <span class="math inline"><em>Q</em></span>-function approximator, powered by the PyTorch ML Library. The network maps a state vector <span class="math inline"><em>S</em></span> to 4 real numbers. When successfully trained, these are the 4 <span class="math inline"><em>Q</em></span>-values for each of the four actions. I was able to solve the environment with a network with only 1 fully connected hidden layer so all agents use the same structure but with a variable size number of nodes. Non-linearity is produced using ReLU between network layers.</p>
<p>Unlike related work, this agent only uses the single network rather than a less frequently updated target network when picking the next action. I was not able to get consistent results using the target network.</p>
<p>A number of agents managed to converge and solve the environment. After this is the agent evaluation phase, where each of these agents were evaluated against another 100 episodes without any training updates. The final agent was the one which scored highly and also consistently landed safely. Surprisingly, a few solved agents actually performed poorly after training, Figure 3 plots the solve agents performance during evaluation. 5 of which did not have an average episode reward above 200. Interestingly, a few of them had very high reward when discounting crashes which indicates they try to land too quickly, which only sometimes pays off depending on starting position.</p>
<figure>
<img src="6_bestEvaluation.png" id="fig:fig2" alt="Best Agent Performance" /><figcaption aria-hidden="true">Best Agent Performance</figcaption>
</figure>
<p>The best performing agent’s train and test performance can be seen in Figure 2. The agent used a network with 320 hidden nodes, an exploration decay rate of 50, and a discount factor of 0.99. The details of these parameters is explained below. This agent was not the fastest to converge but it does have the best evaluation. It is both high scoring, meaning it lands quickly and accurately without costing too much on fuel, and stable, meaning the agent does not crash in its attempts to land quickly.</p>
<p>Even at the end of training the agent still has some very bad episodes, either having negative or low positive cumulative reward. This ends during evaluation, which leads me to believe there were critical times when the agent chose a random action rather than an optimal one and was unable to recover. Evaluation always took what was considered the optimal action.</p>
<figure>
<img src="5_solvedEvaluation.png" id="fig:fig2.5" alt="Solved agent evaluation performances" /><figcaption aria-hidden="true">Solved agent evaluation performances</figcaption>
</figure>
<figure>
<img src="1_convergenceTimeByNetworkSize.png" id="fig:fig3" alt="Solved agent performances by network size" /><figcaption aria-hidden="true">Solved agent performances by network size</figcaption>
</figure>
<h1 id="training-experimentation">Training &amp; Experimentation</h1>
<p>For agent selection, a grid search was performed across 3 hyper-parameters. 14 agents of various configurations were able to solve the environment out of a total 168 candidates, approximately <span class="math inline">8%</span> of agents. The agents had 1000 episodes to solve the environment. Only 2 agents took longer than 500 episodes to solve the environment, so it was a fair cutoff for most agents.</p>
<p>Agents which solved the environment were saved for evaluation later. In addition, a replay buffer was used to decorrelate the data and allow batch network updates. The buffer was fixed at the last 50,000 data points and sampled 128 points at a time. The data points are a tuple of the State, Action, Next State, Reward for taking the action, and optionally the <span class="math inline"><em>τ</em>(<em>S</em>)</span> and <span class="math inline"><em>τ</em>(<em>S</em>′)</span> which was not used during the grid search as I did not find a useful potential function.</p>
<p>For experimental data, each agent recorded both the episode’s cumulative reward and the number of time steps in the episode. If the agent was successful in solving the agent’s network weights and configuration they were saved for evaluation later.</p>
<h2 id="network-size">Network Size</h2>
<p>As mentioned above, the first parameter is network size. The number of nodes in the hidden layer is related to how expressible the network is. Networks of size <span class="math inline">[128,192,256,320]</span> were explored. The hypothesis is that a smaller network should be less flexible but also faster to train as fewer nodes need to be optimized.</p>
<p>In Figure 4, the successfully trained agents can be seen broken down by network size. The largest network, with 320 parameters, had the most solves with 5 total. This leads me to believe that the larger network was more flexible to the other parameters. Both the 320 and 256 also had longer training times for some configurations. However, all network sizes seem to have a few configurations which converge quickly. As a general rule, the larger networks seem to perform better as they have the flexibility to converge under multiple configurations and can converge as quickly as the small network under similar configurations. The supposed downside, that more parameters require more processing power, is not a large concern as the bottleneck during training was the simulated environments rather than the high performance numerical libraries.</p>
<h2 id="epsilon-decay"><span class="math inline"><em>ϵ</em></span>-Decay</h2>
<p>For training, <span class="math inline"><em>ϵ</em></span>-greedy was used to allow additional exploration. During evaluation the agent was allowed to make the optimal action. The <span class="math inline"><em>ϵ</em></span> initially started at 0.95 before eventually decaying down to 0.05. The rate of decay was one of the three hyper-parameters. The <span class="math inline"><em>ϵ</em></span>-decay follows a power-law decay but for most <span class="math inline"><em>ϵ</em></span>-decay values the exploration rate eventually reaches <span class="math inline">0.05</span> due to numerical instability. The decay values used were <span class="math inline">[1,25,50,75,100,125,200]</span>. The exploration rate is dependent on the number of prior episodes and the decay value. As an example, on episode 100, with a decay rate of 25, the exploration rate would be <span class="math inline">0.05 + (0.95−0.05) * <em>e</em><sup>−100/25</sup> ≈ 0.0665</span></p>
<p>The fastest converges occurred at the lowest two decay rates. One agent even managed to solve the model in just under 150 episodes. In addition, the performance is still good during evaluation. This is very surprising as we would expect the low exploration rate to eventually result in bad performance. However, it appears this environment can be learned quite quickly and accurately in a small amount of time with minimal exploration.</p>
<p>On the other side, the largest two decay rates did not result in any solved agents and only a single solution for <span class="math inline"><em>ϵ</em> = 100</span>. This also indicates a quick decay is favored in this environment as large values simple did not converge in time. The larger decay rates also do not guarantee good results during evaluation. Two agents with <span class="math inline"><em>ϵ</em> = 75</span> and <span class="math inline">100</span> failed to score above 200 during evaluation. See Figure 5 for details on these results.</p>
<figure>
<img src="4_solvedByDecay.png" id="fig:fig4" alt="Solved agent performances by Decay Rate." /><figcaption aria-hidden="true">Solved agent performances by Decay Rate.</figcaption>
</figure>
<h2 id="discount-factor">Discount Factor</h2>
<p>The final hyper-parameter was the discount factor of the reward. This value, characterized as <span class="math inline"><em>γ</em></span>, determines the trade-off of future reward to present reward. With a discount of 0.9, a reward of 1 which is 10 steps in the future is only valued at <span class="math inline">0.9<sup>10</sup> ≈ 0.35</span>. A discount of <span class="math inline">1</span> is no discounting. The values used during search were <span class="math inline">[0.85,0.9,0.95,0.99,0.999,1.0]</span>.</p>
<p>Unsurprisingly, 1 was not a good discount. Both 1 and 0.999 had no agents which scored above 0, see Figure 6. The most surprising result is that the only good discount rate was 0.99. No agents were able to converge for any other discount rate. In addition, the agents did not get close. With a discount of 0.95, the best agent only achieve an average score of 38 over 100 episodes. On the other hand, plenty of agents with a discount of 0.99 achieved near solutions (See Figure 6). The discount rate can be viewed as a property of the environment rather than of the training algorithm. Therefore, it seems the optimal rate for Lunar Lander is near 0.99 and is very sensitive to change. However, I leave open the possibility that <span class="math inline"><em>γ</em> = 0.99</span> is only optimal for the hard-coded and untuned hyper-parameters.</p>
<p>From Figure 6 we also see multiple near solutions with <span class="math inline"><em>γ</em> = 0.99</span>. It’s probable some would have converged if given more training time, beyond the 1000 episodes.</p>
<figure>
<img src="3_unsolvedByGamma.png" id="fig:fig5" alt="Unsolved agents by \gamma" /><figcaption aria-hidden="true">Unsolved agents by <span class="math inline"><em>γ</em></span></figcaption>
</figure>
<h2 id="other-parameters">Other Parameters</h2>
<p>There are a number of other hyper-parameters that affected agent performance which were not optimized due to computation constraints. They include: The learning rate of the agents, any other network structures beyond a single fully connected layer, the size of the replay buffer and the sampling size, and the starting and ending exploration rate <span class="math inline"><em>ϵ</em></span>. Even using <span class="math inline"><em>ϵ</em></span>-greedy could be changed, the network outputs could be normalized and treated as probabilities and actions sampled from the resulting distribution.</p>
<h2 id="challenges">Challenges</h2>
<p>There were a number of issues with experimentation. The largest is the performance of the environment. The lunar lander environment is slow to sample from compared to the agent and optimization. This limited the number of features that could be explored. In total, with 168 agents, the experiment took 52 hours to complete during training. I would have liked to find the smallest possible network that solves the environment and tried other network structures.</p>
<p>A byproduct of the performance, I am not sure how stable any individual configuration is to convergence. Because I did not run multiple iterations of training for the same agents, it is possible other agents could have solved if they had been "luckier" in their training. This also might have removed the agents which did solve but did not perform well during evaluation. With additional time, I would have concentrated on the solving and near-solving agents to average their training across multiple restarts.</p>
<p>The agents also supported using a potential function <span class="math inline"><em>τ</em></span>. I tried a number of <span class="math inline"><em>τ</em></span> functions, such as giving reward for maintaining an upright orientation, reward for not being above a certain vertical velocity, and scaling the built in potential reward for proximity to the landing pad. However, these introduced additional hyper parameters to tune. If there was more time I would try using solved models with these <span class="math inline"><em>τ</em></span> functions to find the correct scaling and determine which work well.</p>
<h1 id="conclusion">Conclusion</h1>
<p>These experiments show the challenge an agent faces in generalization. There was a large number of parameter tuning required to simply get a moderately performing agent. In addition, the most important parameter for determining the agents convergence was unexpectedly <span class="math inline"><em>γ</em></span>. Nearly all other parameters had examples of convergence but all required a <span class="math inline"><em>γ</em></span> of <span class="math inline">0.99</span> to even be close. The experimental results around the parameters and what are good values are specific to the Lunar Lander environment. There is no guarantee these values will be optimal for similar environments or different agent algorithms.</p>
<div class="thebibliography">
<p><span>99</span></p>
<p>Volodymyr Mnih, Koray Kavukcuoglu, David Silver, Alex Graves, Ioannis Antonoglou, Daan Wierstra, Martin Riedmiller <em>Playing Atari with Deep Reinforcement Learning</em> <code>https://arxiv.org/abs/1312.5602</code></p>
<p>Greg Brockman and Vicki Cheung and Ludwig Pettersson and Jonas Schneider and John Schulman and Jie Tang and Wojciech Zaremba. <em>OpenAI Gym</em> <code>https://arxiv.org/abs/1606.01540</code></p>
<p>Sutton, R.S. Mach Learn (1988) 3: 9. https://doi.org/10.1007/BF00115009 Learning to predict by the methods of temporal differences</p>
<p>Paszke Adam <em>Reinforcement Learning (DQN) Tutorial</em> https://pytorch.org/tutorials/intermediate/ reinforcement_q_learning.html</p>
</div>
</body>
</html>
